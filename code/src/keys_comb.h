// File containing all the pressed keys.
#include <avr/pgmspace.h>
#include "pin_defines.h"

/* An array of "Command" that contains all the needed keys to be pressed.\n
And \see commands_size is the array size.*/
const Command comandos[] PROGMEM = {
	{ /* Shift  */ (uint8_t)(( WDTO_30MS << 4) |  1),  (uint8_t)((0x05 << 4) | 0x0B), (PB3 << 4) | PD7}, //
	{ /* 9   	*/ (uint8_t)(( WDTO_30MS << 4) |  1),  (uint8_t)((0x08 << 4) | 0x05), (PC4 << 4) | PB2}, //
	{ /* 3   	*/ (uint8_t)(( WDTO_30MS << 4) |  1),  (uint8_t)((0x08 << 4) | 0x05), (PC4 << 4) | PB0}, 
	{ /* =   	*/ (uint8_t)(( WDTO_30MS << 4) |  1),  (uint8_t)((0x08 << 4) | 0x05), (PC2 << 4) | PB0}, //
	{ /* AC   	*/ (uint8_t)(( WDTO_30MS << 4) |  1),  (uint8_t)((0x08 << 4) | 0x05), (PC0 << 4) | PB2}, //
	{ /* Shift  */ (uint8_t)(( WDTO_30MS << 4) |  1),  (uint8_t)((0x05 << 4) | 0x0B), (PB3 << 4) | PD7}, //
	{ /* +   	*/ (uint8_t)(( WDTO_30MS << 4) |  1),  (uint8_t)((0x08 << 4) | 0x05), (PC3 << 4) | PB0}, //Pol
	{ /* 1   	*/ (uint8_t)(( WDTO_30MS << 4) |  1),  (uint8_t)((0x05 << 4) | 0x05), (PB3 << 4) | PB0}, //
	{ /* Shift  */ (uint8_t)(( WDTO_30MS << 4) |  1),  (uint8_t)((0x05 << 4) | 0x0B), (PB3 << 4) | PD7}, //
	{ /* )   	*/ (uint8_t)(( WDTO_30MS << 4) |  1),  (uint8_t)((0x08 << 4) | 0x05), (PC3 << 4) | PB6}, //, 
	{ /* 0   	*/ (uint8_t)(( WDTO_30MS << 4) |  1),  (uint8_t)((0x08 << 4) | 0x0B), (PC2 << 4) | PD4}, //
	{ /* =   	*/ (uint8_t)(( WDTO_30MS << 4) |  1),  (uint8_t)((0x08 << 4) | 0x05), (PC2 << 4) | PB0}, //
	{ /* O/O   	*/ (uint8_t)(( WDTO_30MS << 4) | 10),  (uint8_t)((0x05 << 4) | 0x0B), (PB3 << 4) | PD5}, //
	{ /* =   	*/ (uint8_t)(( WDTO_30MS << 4) |  1),  (uint8_t)((0x08 << 4) | 0x05), (PC2 << 4) | PB0}, //
	{ /* AC   	*/ (uint8_t)(( WDTO_30MS << 4) |  1),  (uint8_t)((0x08 << 4) | 0x05), (PC0 << 4) | PB2}, //
	{ /* <   	*/ (uint8_t)(( WDTO_30MS << 4) |  1),  (uint8_t)((0x08 << 4) | 0x0B), (PC4 << 4) | PD6}, //
	{ /* 1   	*/ (uint8_t)(( WDTO_30MS << 4) |  1),  (uint8_t)((0x05 << 4) | 0x05), (PB3 << 4) | PB0}, //
	{ /* O^O   	*/ (uint8_t)(( WDTO_30MS << 4) |  1),  (uint8_t)((0x08 << 4) | 0x0B), (PC3 << 4) | PD5}, //
	{ /* =   	*/ (uint8_t)(( WDTO_30MS << 4) |  1),  (uint8_t)((0x08 << 4) | 0x05), (PC2 << 4) | PB0}, //
	{ /* AC   	*/ (uint8_t)(( WDTO_30MS << 4) |  1),  (uint8_t)((0x08 << 4) | 0x05), (PC0 << 4) | PB2}, //
	{ /* ˄   	*/ (uint8_t)(( WDTO_30MS << 4) |  1),  (uint8_t)((0x08 << 4) | 0x0B), (PC4 << 4) | PD7}, //
	{ /* AC   	*/ (uint8_t)(( WDTO_60MS << 4) |  1),  (uint8_t)((0x08 << 4) | 0x05), (PC0 << 4) | PB2}, //
	{ /* <   	*/ (uint8_t)(( WDTO_60MS << 4) |  3),  (uint8_t)((0x08 << 4) | 0x0B), (PC4 << 4) | PD6}, //
	{ /* DEL   	*/ (uint8_t)(( WDTO_60MS << 4) |  1),  (uint8_t)((0x08 << 4) | 0x05), (PC3 << 4) | PB2}, //
	{ /* sqrt(O)*/ (uint8_t)(( WDTO_60MS << 4) |  1),  (uint8_t)((0x08 << 4) | 0x0B), (PC5 << 4) | PD5}, //
	{ /* 1   	*/ (uint8_t)(( WDTO_60MS << 4) |  1),  (uint8_t)((0x05 << 4) | 0x05), (PB3 << 4) | PB0}, //
	{ /* M+   	*/ (uint8_t)(( WDTO_30MS << 4) |  1),  (uint8_t)((0x08 << 4) | 0x05), (PC1 << 4) | PB6}, //
	{ /* AC   	*/ (uint8_t)(( WDTO_30MS << 4) |  1),  (uint8_t)((0x08 << 4) | 0x05), (PC0 << 4) | PB2}, //
	{ /* Ans   	*/ (uint8_t)(( WDTO_30MS << 4) | 10),  (uint8_t)((0x08 << 4) | 0x05), (PC2 << 4) | PB1}, //
	{ /* Log_O( */ (uint8_t)(( WDTO_30MS << 4) |  1),  (uint8_t)((0x08 << 4) | 0x0B), (PC1 << 4) | PD6}, //
	{ /* Abs   	*/ (uint8_t)(( WDTO_30MS << 4) |  1),  (uint8_t)((0x05 << 4) | 0x0B), (PB3 << 4) | PD6}, //
	{ /* >      */ (uint8_t)(( WDTO_30MS << 4) |  2),  (uint8_t)((0x08 << 4) | 0x0B), (PB3 << 4) | PD7}, 
	{ /* (   	*/ (uint8_t)(( WDTO_30MS << 4) | 10),  (uint8_t)((0x08 << 4) | 0x05), (PC4 << 4) | PB6}, //	
	{ /* =   	*/ (uint8_t)(( WDTO_30MS << 4) |  1),  (uint8_t)((0x08 << 4) | 0x05), (PC2 << 4) | PB0}, //
	{ /* <   	*/ (uint8_t)(( WDTO_30MS << 4) |  2),  (uint8_t)((0x08 << 4) | 0x0B), (PC4 << 4) | PD6}, //
	{ /* sin(   */ (uint8_t)(( WDTO_30MS << 4) |  7),  (uint8_t)((0x08 << 4) | 0x0B), (PC3 << 4) | PD4}, //
	{ /* =   	*/ (uint8_t)(( WDTO_30MS << 4) |  1),  (uint8_t)((0x08 << 4) | 0x05), (PC2 << 4) | PB0}, //
	{ /* <   	*/ (uint8_t)(( WDTO_30MS << 4) |  2),  (uint8_t)((0x08 << 4) | 0x0B), (PC4 << 4) | PD6}, //
	{ /* (   	*/ (uint8_t)(( WDTO_30MS << 4) |  7),  (uint8_t)((0x08 << 4) | 0x05), (PC4 << 4) | PB6}, //
	{ /* =   	*/ (uint8_t)(( WDTO_30MS << 4) |  1),  (uint8_t)((0x08 << 4) | 0x05), (PC2 << 4) | PB0}, //
	{ /* <   	*/ (uint8_t)(( WDTO_30MS << 4) |  2),  (uint8_t)((0x08 << 4) | 0x0B), (PC4 << 4) | PD6}, ///
	
	{ /* (   	*/ (uint8_t)((WDTO_120MS << 4) |  6),  (uint8_t)((0x08 << 4) | 0x05), (PC4 << 4) | PB6}, //
	{ /* =   	*/ (uint8_t)((WDTO_120MS << 4) |  1),  (uint8_t)((0x08 << 4) | 0x05), (PC2 << 4) | PB0}, //
	{ /* <   	*/ (uint8_t)((WDTO_120MS << 4) |  1),  (uint8_t)((0x08 << 4) | 0x0B), (PC4 << 4) | PD6}, ///
	
	{ /* (   	*/ (uint8_t)((WDTO_500MS << 4) |  6),  (uint8_t)((0x08 << 4) | 0x05), (PC4 << 4) | PB6}, //
	{ /* =   	*/ (uint8_t)((WDTO_120MS << 4) |  1),  (uint8_t)((0x08 << 4) | 0x05), (PC2 << 4) | PB0}, //
	{ /* <   	*/ (uint8_t)((WDTO_120MS << 4) |  1),  (uint8_t)((0x08 << 4) | 0x0B), (PC4 << 4) | PD6}, ///
	
	{ /* (   	*/ (uint8_t)((WDTO_500MS << 4) |  6),  (uint8_t)((0x08 << 4) | 0x05), (PC4 << 4) | PB6}, //
	{ /* =   	*/ (uint8_t)((WDTO_120MS << 4) |  1),  (uint8_t)((0x08 << 4) | 0x05), (PC2 << 4) | PB0}, //
	{ /* <   	*/ (uint8_t)((WDTO_120MS << 4) |  1),  (uint8_t)((0x08 << 4) | 0x0B), (PC4 << 4) | PD6}, ///
	
	{ /* (   	*/ (uint8_t)((WDTO_500MS << 4) |  6),  (uint8_t)((0x08 << 4) | 0x05), (PC4 << 4) | PB6}, //
	{ /* =   	*/ (uint8_t)((WDTO_500MS << 4) |  1),  (uint8_t)((0x08 << 4) | 0x05), (PC2 << 4) | PB0}, //
	{ /* <   	*/ (uint8_t)((WDTO_500MS << 4) |  1),  (uint8_t)((0x08 << 4) | 0x0B), (PC4 << 4) | PD6}, ///
	
	{ /* (   	*/ (uint8_t)((WDTO_1S    << 4) |  6),  (uint8_t)((0x08 << 4) | 0x05), (PC4 << 4) | PB6}, //
	{ /* =   	*/ (uint8_t)((WDTO_500MS << 4) |  1),  (uint8_t)((0x08 << 4) | 0x05), (PC2 << 4) | PB0}, //
	{ /* <   	*/ (uint8_t)((WDTO_1S    << 4) |  1),  (uint8_t)((0x08 << 4) | 0x0B), (PC4 << 4) | PD6}, ///
	
	{ /* (   	*/ (uint8_t)((WDTO_1S    << 4) |  6),  (uint8_t)((0x08 << 4) | 0x05), (PC4 << 4) | PB6}, //
	{ /* =   	*/ (uint8_t)((WDTO_500MS << 4) |  1),  (uint8_t)((0x08 << 4) | 0x05), (PC2 << 4) | PB0}, //
	{ /* <   	*/ (uint8_t)((WDTO_1S    << 4) |  1),  (uint8_t)((0x08 << 4) | 0x0B), (PC4 << 4) | PD6}, ///
	
	{ /* (   	*/ (uint8_t)((WDTO_2S    << 4) |  6),  (uint8_t)((0x08 << 4) | 0x05), (PC4 << 4) | PB6}, //
	{ /* =   	*/ (uint8_t)((WDTO_500MS << 4) |  1),  (uint8_t)((0x08 << 4) | 0x05), (PC2 << 4) | PB0}, //
	{ /* <   	*/ (uint8_t)((WDTO_2S    << 4) |  1),  (uint8_t)((0x08 << 4) | 0x0B), (PC4 << 4) | PD6}, ///
	
	{ /* (   	*/ (uint8_t)((WDTO_2S    << 4) |  5),  (uint8_t)((0x08 << 4) | 0x05), (PC4 << 4) | PB6}, //
	{ /* =   	*/ (uint8_t)((WDTO_500MS << 4) |  1),  (uint8_t)((0x08 << 4) | 0x05), (PC2 << 4) | PB0}, //
	{ /* <   	*/ (uint8_t)((WDTO_120MS << 4) |  1),  (uint8_t)((0x08 << 4) | 0x0B), (PC4 << 4) | PD6}, ///
	
	{ /* Shift  */ (uint8_t)((WDTO_120MS << 4) |  1),  (uint8_t)((0x05 << 4) | 0x0B), (PB3 << 4) | PD7}, //
	{ /* 9   	*/ (uint8_t)(( WDTO_30MS << 4) |  1),  (uint8_t)((0x08 << 4) | 0x05), (PC4 << 4) | PB2}, //
	{ /* 1   	*/ (uint8_t)(( WDTO_30MS << 4) |  1),  (uint8_t)((0x05 << 4) | 0x05), (PB3 << 4) | PB0}, //
	{ /* =   	*/ (uint8_t)(( WDTO_30MS << 4) |  1),  (uint8_t)((0x08 << 4) | 0x05), (PC2 << 4) | PB0}, //
	{ /* AC   	*/ (uint8_t)(( WDTO_30MS << 4) |  1),  (uint8_t)((0x08 << 4) | 0x05), (PC0 << 4) | PB2}, //
	{ /* Shift  */ (uint8_t)(( WDTO_30MS << 4) |  1),  (uint8_t)((0x05 << 4) | 0x0B), (PB3 << 4) | PD7}, //
	{ /* 9   	*/ (uint8_t)(( WDTO_30MS << 4) |  1),  (uint8_t)((0x08 << 4) | 0x05), (PC4 << 4) | PB2}, //
	{ /* 2   	*/ (uint8_t)(( WDTO_30MS << 4) |  1),  (uint8_t)((0x08 << 4) | 0x05), (PC5 << 4) | PB0}, //
	{ /* =   	*/ (uint8_t)(( WDTO_30MS << 4) |  1),  (uint8_t)((0x08 << 4) | 0x05), (PC2 << 4) | PB0}, //
	{ /* AC   	*/ (uint8_t)(( WDTO_30MS << 4) |  1),  (uint8_t)((0x08 << 4) | 0x05), (PC0 << 4) | PB2}, //
	{ /* MODE   */ (uint8_t)(( WDTO_30MS << 4) |  2),  (uint8_t)((0x08 << 4) | 0x0B), (PC0 << 4) | PD7}, //
	{ /* Shift  */ (uint8_t)(( WDTO_30MS << 4) |  1),  (uint8_t)((0x05 << 4) | 0x0B), (PB3 << 4) | PD7}, //
	{ /* MODE   */ (uint8_t)(( WDTO_30MS << 4) |  1),  (uint8_t)((0x08 << 4) | 0x0B), (PC0 << 4) | PD7}, //
	{ /* ˅   	*/ (uint8_t)(( WDTO_30MS << 4) |  1),  (uint8_t)((0x08 << 4) | 0x0B), (PC3 << 4) | PD6}, //
	{ /* 6   	*/ (uint8_t)(( WDTO_30MS << 4) |  1),  (uint8_t)((0x08 << 4) | 0x05), (PC4 << 4) | PB1}, //
	{ /* <   	*/ (uint8_t)(( WDTO_30MS << 4) | 10),  (uint8_t)((0x08 << 4) | 0x0B), (PC4 << 4) | PD6}, //
	{ /* AC   	*/ (uint8_t)(( WDTO_30MS << 4) |  1),  (uint8_t)((0x08 << 4) | 0x05), (PC0 << 4) | PB2}, //
	{ /* Shift  */ (uint8_t)(( WDTO_30MS << 4) |  1),  (uint8_t)((0x05 << 4) | 0x0B), (PB3 << 4) | PD7}, //
	{ /* MODE   */ (uint8_t)(( WDTO_30MS << 4) |  1),  (uint8_t)((0x08 << 4) | 0x0B), (PC0 << 4) | PD7}, //
	{ /* 2   	*/ (uint8_t)(( WDTO_30MS << 4) |  1),  (uint8_t)((0x08 << 4) | 0x05), (PC5 << 4) | PB0}, //
	{ /* AC   	*/ (uint8_t)(( WDTO_30MS << 4) |  1),  (uint8_t)((0x08 << 4) | 0x05), (PC0 << 4) | PB2}, //
};

const uint8_t comandos_size = sizeof(comandos)/sizeof(Command);
