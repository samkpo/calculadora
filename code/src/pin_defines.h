#ifndef DEFINES_H
#define DEFINES_H

//AVR pins i/o
#include <avr/io.h>

/**
 * @brief      This struct contains the calculator "buttons" combination.
 * 
 * That combination are the row and column that have to be connected so the
 * calculator ttakes the bvutton as being pressed.
 * 
 * So we need to define two pins on the atmega and the time it needs to be
 * connected so the calc is able to read it.
 * 
 * Available times for the struct:
 * #define 	WDTO_15MS   0
 * #define 	WDTO_30MS   1
 * #define 	WDTO_60MS   2
 * #define 	WDTO_120MS   3
 * #define 	WDTO_250MS   4
 * #define 	WDTO_500MS   5
 * #define 	WDTO_1S   6
 * #define 	WDTO_2S   7
 * #define 	WDTO_4S   8
 * #define 	WDTO_8S   9
 * 
 * 
 * The atmega328p ports are declared as follows:
 * 
 * \code
 * #define PORTB _SFR_IO8(0x05)
 * #define PORTC _SFR_IO8(0x08)
 * #define PORTD _SFR_IO8(0x0B)
 * \endcode
 * 
 * So we can store that value in the array, and in the loop get the PORT, saving
 * us a couple of bits
 * 
 */

struct Command
{
	uint8_t time_repeat;       ///< Repeat number lower nibble, time in upper one
	uint8_t ports;			   ///< Contains both ports
	uint8_t rc_pin;            ///< Row pin on upper nibble, col pin on lower one
};
/*The pin will be set as this:
	0b xxxx xxxx
		row  col

  byte = (row_pin << 4) | (col_pin);
 */


#endif
