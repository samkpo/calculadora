#ifndef GLOBAL_DEFINES_H
#define GLOBAL_DEFINES_H

// An anonymous namespace restricts these variables to the scope of the
// compilation unit.
namespace {
#ifdef DEBUG
  	const char* PROJECT_LONGNAME = "@PROJECT_LONGNAME@";
  	const char* PROJECT_VERSION = "@PROJECT_VERSION@";
  	const uint32_t BAUD = @BAUD_RATE@;
#endif
}

#endif
