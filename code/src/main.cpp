#include <avr/interrupt.h>
#include <avr/sleep.h>
#include <avr/wdt.h>
#include <avr/power.h>
#include <avr/pgmspace.h>
#include <util/delay.h>
#ifndef TLED
#include "keys_comb.h"
#endif
#include "pin_defines.h"

/**
 * \todo use a Makefile
 */

////////////////////////////////////////////////////////////////////////////////
//////////////////////////// Function declarations. ////////////////////////////
////////////////////////////////////////////////////////////////////////////////
//External interruption 0
inline void configure_EINT0();

//Check watchdog status
inline void check_wdt();

//Enable watchdog
inline void init_wdt();

//Disable watchdog
inline void wdt_off();

//Change watchdog time
inline void wdt_prescaler_change(uint8_t value);

//Configure pins and ios
inline void configure_ios();

//Turn off uC
void powerDown();

// This function is called upon a HARDWARE RESET:
//void wdt_first(void) __attribute__((naked)) __attribute__((section(".init3")));

///////////////////////////////////////////////////////////////////////////////
/////////////////////////////// Global variable ///////////////////////////////
///////////////////////////////////////////////////////////////////////////////
#ifdef TLED
#define CURRENT_TIME WDTO_120MS
#else
#define CURRENT_TIME WDTO_250MS
#endif

// struct Command {
// 	uint8_t         repeat;   ///< The number of times the key has to be pressed
// 	volatile uint8_t* row_port; ///< Pointer to the pin port of the column.
// 	uint8_t         row_pin;  ///< Port bit of the pin of the column.
// 	volatile uint8_t* col_port; ///< Pointer to the pin port of the row.
// 	uint8_t         col_pin;  ///< Port bit of the pin of the row.
// };
#ifdef TLED
const Command comandos[] PROGMEM = {
	{(WDTO_120MS << 4) | (5), (uint8_t)((0x05 << 4) |0x05), (PB3 << 4)|(PB4) },
	{(WDTO_500MS << 4) | (5), (uint8_t)((0x05 << 4) |0x05), (PB4 << 4)|(PB6) },
	{(WDTO_120MS << 4) | (5), (uint8_t)((0x05 << 4) |0x0B), (PB6 << 4)|(PD7) },
	{(WDTO_120MS << 4) | (5), (uint8_t)((0x0B << 4) |0x05), (PD7 << 4)|(PB3) }
};
const uint8_t comandos_size = sizeof(comandos)/sizeof(Command);
#endif

////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////// Main /////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
int main (void)
{
	//Setup pins
	configure_ios();

	//Check watchdog status
	cli();
	check_wdt();
	init_wdt();
	sei();

	//Configure External interrupt
	configure_EINT0();

	//TODO: Check if these are consuming energy while the uC is on
	//Bye ADC
	ADCSRA = 0;
	//Bye SPI
	power_spi_disable();
	//Bye TWI
	power_twi_disable();

	//Configure power down status
	// Enable Sleep Mode for Power Down
	set_sleep_mode(SLEEP_MODE_PWR_DOWN);    // Set Sleep Mode: Power Down

	//Pointer needed to work
	while(1){
    	//Power down uC
    	powerDown();

		for (uint8_t i = 0; i < comandos_size; ++i)
		{
			//Get ports (volatile uint8_t*)
			uint8_t _port = pgm_read_byte(&comandos[i].ports);
			decltype(PORTB) row_port = _SFR_IO8((0xf0 & _port) >> 4);
			decltype(PORTB) col_port = _SFR_IO8(0x0f & _port);

			//Get repeat number and time
			uint8_t r_t = pgm_read_byte(&comandos[i].time_repeat);
			uint8_t time    = ((0xf0 & r_t) >> 4) ;
			uint8_t counter = (0x0f & r_t);

			//Get column and row pin
			uint8_t rc_pin  = pgm_read_byte(&comandos[i].rc_pin);
			uint8_t row_bit = ((0xf0 & rc_pin) >> 4) ;
			uint8_t col_bit = (0x0f & rc_pin);

			for (uint8_t j = 0; j < counter; ++j)
			{
				//Turn on
				wdt_prescaler_change(WDTO_250MS);
				row_port ^= (1 << row_bit);
				col_port ^= (1 << col_bit);
				powerDown();

				//Turn off
				wdt_prescaler_change(time);
				row_port ^= (1 << row_bit);
				col_port ^= (1 << col_bit);
				powerDown();
			}
		}

		//After the big loop, we Turn the watchdog off, enable the interrupts so
		//when the user presses the button, it restarts
		wdt_off();
	}
}

///////////////////////////////////////////////////////////////////////////////
////////////////////////////// Interrupt vectors //////////////////////////////
///////////////////////////////////////////////////////////////////////////////
//External interrupt
ISR(INT1_vect)
{
	cli();

	//Little delay
	_delay_ms(50);

	//Check if still low
	if(!(PIND & _BV(3))){
		//We must enable the watchdog in here
		if(!(WDTCSR & (1 << WDIE))) //wdt interrupts are disabled
			init_wdt();
	}

	sei();
}

ISR(WDT_vect){}

////////////////////////////////////////////////////////////////////////////////
/////////////////////////// Function implementations ///////////////////////////
////////////////////////////////////////////////////////////////////////////////
inline void configure_EINT0()
{
	//Disable interrupts
	cli();
	
	//It's on pin PORTD2;
	DDRD &= ~(1 << DDD3);     // Clear the PD3 pin
    // PD3 (PCINT0 pin) is now an input

    PORTD |= (1 << PORTD3);    // turn On the Pull-up
    // PD3 is now an input with pull-up enabled
    
    EICRA |= (3 << ISC10);    // set INT0 to trigger on FALLING
    EIMSK |= (1 << INT1);     // Turns on INT0

    sei();                    // turn on interrupts
}

void powerDown()
{
	cli();
	sleep_enable();
	sleep_bod_disable();
	sei();
	sleep_cpu();
	//Here the program goes on
	sleep_disable();
	sei();
}

inline void configure_ios()
{
#ifdef TLED
	//Leds
	/* set pin 5 of PORTB for output*/
	DDRB  |= (1 << 3) | (1 << 4) | (1 << 5) | (1 << 6);
	PORTB |= (1 << 3) | (1 << 4) | (1 << 6); //Turn off leds 11 and 12
	DDRD  |= (1 << 7);
	PORTD |= (1 << 7);
#else
	/* Configure PORT B */
	DDRB |= (1 << PB0) | (1 << PB1) | (1 << PB2) | (1 << PB3) | (1 << PB4) | (1 << PB5) | (1 << PB6);
	PORTB &= ~((1 << PB0) | (1 << PB1) | (1 << PB2) | (1 << PB3) | (1 << PB4) | (1 << PB6));
	PORTB |= (1 << PB5);

	/* Configure PORT C */
	DDRC |= (1 << PC0) | (1 << PC1) | (1 << PC2) | (1 << PC3) | (1 << PC4) | (1 << PC5);
	PORTC &= ~((1 << PC0) | (1 << PC1) | (1 << PC2) | (1 << PC3) | (1 << PC4) | (1 << PC5));

	/* Configure PORT D */
	DDRD |= (1 << PD4) | (1 << PD5) | (1 << PD6) | (1 << PD7);
	PORTD &= ~((1 << PD4) | (1 << PD5) | (1 << PD6) | (1 << PD7));
#endif	
}

inline void check_wdt()
{
	if(MCUSR & (1 << WDRF)){            // If a reset was caused by the Watchdog Timer...
 		MCUSR &= ~(1 << WDRF);                // Clear the WDT reset flag
    	WDTCSR = 0x00;                      // Disable the WDT
   	}
}

/**
 *	Setting the watchdog pre-scaler value with VCC = 5.0V and 16mHZ
 *	WDP3 WDP2 WDP1 WDP0 | uint8_t | Number of WDT | Typical | WDT Macro
 *	0    0    0    0    |    0    |   2K cycles   | 16 ms   | WDTO_15MS
 *	0    0    0    1    |    1    |   4K cycles   | 32 ms   | WDTO_30MS
 *	0    0    1    0    |    2    |   8K cycles   | 64 ms   | WDTO_60MS
 *	0    0    1    1    |    3    |  16K cycles   | 0.125 s | WDTO_120MS
 *	0    1    0    0    |    4    |  32K cycles   | 0.25 s  | WDTO_250MS
 *	0    1    0    1    |    5    |  64K cycles   | 0.5 s   | WDTO_500MS
 *	0    1    1    0    |    6    |  128K cycles  | 1.0 s   | WDTO_1S
 *	0    1    1    1    |    7    |  256K cycles  | 2.0 s   | WDTO_2S
 *	1    0    0    0    |    8    |  512K cycles  | 4.0 s   | WDTO_4S
 *	1    0    0    1    |    9    | 1024K cycles  | 8.0 s   | WDTO_8S
 */
inline void init_wdt()
{
	// Set up Watch Dog Timer for Inactivity
    WDTCSR |= (1 << WDCE) | (1 << WDE);              // Enable the WD Change Bit
                                                     // Enable WDT interrupt 
    WDTCSR =  (1 << WDIE) | CURRENT_TIME;   // Set Timeout to ~500 ms
}

inline void wdt_prescaler_change(uint8_t value)
{
	cli();
	wdt_reset();

	/* Start timed sequence */
	WDTCSR |= (1 << WDCE) | (1 << WDE);
	/* Set new prescaler(time-out) value = 64K cycles (~0.5 s) */
	WDTCSR  = (1 << WDIE) | value;

	sei();
}

inline void wdt_off()
{
	cli();
	wdt_reset();

	/* Clear WDRF in MCUSR */
	MCUSR &= ~(1 << WDRF);

	/* Turn off WDT */
	WDTCSR = 0x00;

	sei();
}

