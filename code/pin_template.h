// File containing all the pressed keys.
#include <avr/pgmspace.h>
#include "pin_defines.h"

/* An array of "Command" that contains all the needed keys to be pressed.\n
And \see commands_size is the array size.*/
const Command comandos[] PROGMEM = {
	{ /* Shift  */ (uint8_t)((WDTO_120MS << 4) |  1),  (uint8_t)((k1 << 4) | ki8), (L1 << 4) | J8},//
	{ /* 9   	*/ (uint8_t)((WDTO_120MS << 4) |  1),  (uint8_t)((k3 << 4) | ki3), (L3 << 4) | J3},//
	{ /* 3   	*/ (uint8_t)((WDTO_120MS << 4) |  1),  (uint8_t)((k3 << 4) | ki1), (L3 << 4) | J1},
	{ /* =   	*/ (uint8_t)((WDTO_120MS << 4) |  1),  (uint8_t)((k5 << 4) | ki1), (L5 << 4) | J1},//
	{ /* AC   	*/ (uint8_t)((WDTO_120MS << 4) |  1),  (uint8_t)((k7 << 4) | ki3), (L7 << 4) | J3},//
	{ /* Shift  */ (uint8_t)((WDTO_120MS << 4) |  1),  (uint8_t)((k1 << 4) | ki8), (L1 << 4) | J8},//
	{ /* +   	*/ (uint8_t)((WDTO_120MS << 4) |  1),  (uint8_t)((k4 << 4) | ki1), (L4 << 4) | J1}, //Pol
	{ /* 1   	*/ (uint8_t)((WDTO_120MS << 4) |  1),  (uint8_t)((k1 << 4) | ki1), (L1 << 4) | J1},//
	{ /* Shift  */ (uint8_t)((WDTO_120MS << 4) |  1),  (uint8_t)((k1 << 4) | ki8), (L1 << 4) | J8},//
	{ /* )   	*/ (uint8_t)((WDTO_120MS << 4) |  1),  (uint8_t)((k4 << 4) | ki4), (L4 << 4) | J4}, //,
	{ /* 0   	*/ (uint8_t)((WDTO_120MS << 4) |  1),  (uint8_t)((k5 << 4) | ki5), (L5 << 4) | J5},//
	{ /* =   	*/ (uint8_t)((WDTO_120MS << 4) |  1),  (uint8_t)((k5 << 4) | ki1), (L5 << 4) | J1},//
	{ /* O/O   	*/ (uint8_t)((WDTO_250MS << 4) | 10),  (uint8_t)((k1 << 4) | ki6), (L1 << 4) | J6},//
	{ /* =   	*/ (uint8_t)((WDTO_250MS << 4) |  1),  (uint8_t)((k5 << 4) | ki1), (L5 << 4) | J1},//
	{ /* AC   	*/ (uint8_t)((WDTO_500MS << 4) |  1),  (uint8_t)((k7 << 4) | ki3), (L7 << 4) | J3},//
	{ /* <   	*/ (uint8_t)((WDTO_500MS << 4) |  1),  (uint8_t)((k3 << 4) | ki7), (L3 << 4) | J7},//
	{ /* 1   	*/ (uint8_t)((WDTO_500MS << 4) |  1),  (uint8_t)((k1 << 4) | ki1), (L1 << 4) | J1},//
	{ /* O^O   	*/ (uint8_t)((WDTO_250MS << 4) |  1),  (uint8_t)((k4 << 4) | ki6), (L4 << 4) | J6},//
	{ /* =   	*/ (uint8_t)((WDTO_250MS << 4) |  1),  (uint8_t)((k5 << 4) | ki1), (L5 << 4) | J1},//
	{ /* AC   	*/ (uint8_t)((WDTO_250MS << 4) |  1),  (uint8_t)((k7 << 4) | ki3), (L7 << 4) | J3},//
	{ /* ˄   	*/ (uint8_t)((WDTO_250MS << 4) |  1),  (uint8_t)((k3 << 4) | ki8), (L3 << 4) | J8},//
	{ /* AC   	*/ (uint8_t)((WDTO_250MS << 4) |  1),  (uint8_t)((k7 << 4) | ki3), (L7 << 4) | J3},//
	{ /* <   	*/ (uint8_t)((WDTO_250MS << 4) |  3),  (uint8_t)((k3 << 4) | ki7), (L3 << 4) | J7},//
	{ /* DEL   	*/ (uint8_t)((WDTO_250MS << 4) |  1),  (uint8_t)((k4 << 4) | ki3), (L4 << 4) | J3},//
	{ /* sqrt(O)*/ (uint8_t)((WDTO_250MS << 4) |  1),  (uint8_t)((k2 << 4) | ki6), (L2 << 4) | J6},//
	{ /* 1   	*/ (uint8_t)((WDTO_250MS << 4) |  1),  (uint8_t)((k1 << 4) | ki1), (L1 << 4) | J1},//
	{ /* M+   	*/ (uint8_t)((WDTO_250MS << 4) |  1),  (uint8_t)((k6 << 4) | ki4), (L6 << 4) | J4},//
	{ /* AC   	*/ (uint8_t)((WDTO_250MS << 4) |  1),  (uint8_t)((k7 << 4) | ki3), (L7 << 4) | J3},//
	{ /* Ans   	*/ (uint8_t)((WDTO_120MS << 4) | 10),  (uint8_t)((k5 << 4) | ki2), (L5 << 4) | J2},//
	{ /* Log_O( */ (uint8_t)((WDTO_120MS << 4) |  1),  (uint8_t)((k6 << 4) | ki7), (L6 << 4) | J7},//
	{ /* Abs   	*/ (uint8_t)((WDTO_120MS << 4) |  1),  (uint8_t)((k1 << 4) | ki7), (L1 << 4) | J7},//
	{ /* >      */ (uint8_t)((WDTO_120MS << 4) |  2),  (uint8_t)((k4 << 4) | ki8), (L1 << 4) | J8},
	{ /* (   	*/ (uint8_t)((WDTO_120MS << 4) | 10),  (uint8_t)((k3 << 4) | ki4), (L3 << 4) | J4},//	
	{ /* =   	*/ (uint8_t)((WDTO_120MS << 4) |  1),  (uint8_t)((k5 << 4) | ki1), (L5 << 4) | J1},//
	{ /* <   	*/ (uint8_t)((WDTO_120MS << 4) |  2),  (uint8_t)((k3 << 4) | ki7), (L3 << 4) | J7},//
	{ /* sin(   */ (uint8_t)((WDTO_120MS << 4) |  7),  (uint8_t)((k4 << 4) | ki5), (L4 << 4) | J5},//
	{ /* =   	*/ (uint8_t)((WDTO_120MS << 4) |  1),  (uint8_t)((k5 << 4) | ki1), (L5 << 4) | J1},//
	{ /* <   	*/ (uint8_t)((WDTO_120MS << 4) |  2),  (uint8_t)((k3 << 4) | ki7), (L3 << 4) | J7},//
	{ /* (   	*/ (uint8_t)((WDTO_120MS << 4) |  7),  (uint8_t)((k3 << 4) | ki4), (L3 << 4) | J4},//
	{ /* =   	*/ (uint8_t)((WDTO_500MS << 4) |  1),  (uint8_t)((k5 << 4) | ki1), (L5 << 4) | J1},//
	{ /* <   	*/ (uint8_t)((WDTO_500MS << 4) |  2),  (uint8_t)((k3 << 4) | ki7), (L3 << 4) | J7},///
	
	{ /* (   	*/ (uint8_t)((WDTO_250MS << 4) |  6),  (uint8_t)((k3 << 4) | ki4), (L3 << 4) | J4},//
	{ /* =   	*/ (uint8_t)((WDTO_250MS << 4) |  1),  (uint8_t)((k5 << 4) | ki1), (L5 << 4) | J1},//
	{ /* <   	*/ (uint8_t)((WDTO_250MS << 4) |  1),  (uint8_t)((k3 << 4) | ki7), (L3 << 4) | J7},///
	
	{ /* (   	*/ (uint8_t)((WDTO_500MS << 4) |  6),  (uint8_t)((k3 << 4) | ki4), (L3 << 4) | J4},//
	{ /* =   	*/ (uint8_t)((WDTO_250MS << 4) |  1),  (uint8_t)((k5 << 4) | ki1), (L5 << 4) | J1},//
	{ /* <   	*/ (uint8_t)((WDTO_250MS << 4) |  1),  (uint8_t)((k3 << 4) | ki7), (L3 << 4) | J7},///
	
	{ /* (   	*/ (uint8_t)((WDTO_500MS << 4) |  6),  (uint8_t)((k3 << 4) | ki4), (L3 << 4) | J4},//
	{ /* =   	*/ (uint8_t)((WDTO_250MS << 4) |  1),  (uint8_t)((k5 << 4) | ki1), (L5 << 4) | J1},//
	{ /* <   	*/ (uint8_t)((WDTO_250MS << 4) |  1),  (uint8_t)((k3 << 4) | ki7), (L3 << 4) | J7},///
	
	{ /* (   	*/ (uint8_t)((WDTO_500MS << 4) |  6),  (uint8_t)((k3 << 4) | ki4), (L3 << 4) | J4},//
	{ /* =   	*/ (uint8_t)((WDTO_500MS << 4) |  1),  (uint8_t)((k5 << 4) | ki1), (L5 << 4) | J1},//
	{ /* <   	*/ (uint8_t)((WDTO_500MS << 4) |  1),  (uint8_t)((k3 << 4) | ki7), (L3 << 4) | J7},///
	
	{ /* (   	*/ (uint8_t)((WDTO_1S    << 4) |  6),  (uint8_t)((k3 << 4) | ki4), (L3 << 4) | J4},//
	{ /* =   	*/ (uint8_t)((WDTO_500MS << 4) |  1),  (uint8_t)((k5 << 4) | ki1), (L5 << 4) | J1},//
	{ /* <   	*/ (uint8_t)((WDTO_1S    << 4) |  1),  (uint8_t)((k3 << 4) | ki7), (L3 << 4) | J7},///
	
	{ /* (   	*/ (uint8_t)((WDTO_1S    << 4) |  6),  (uint8_t)((k3 << 4) | ki4), (L3 << 4) | J4},//
	{ /* =   	*/ (uint8_t)((WDTO_500MS << 4) |  1),  (uint8_t)((k5 << 4) | ki1), (L5 << 4) | J1},//
	{ /* <   	*/ (uint8_t)((WDTO_1S    << 4) |  1),  (uint8_t)((k3 << 4) | ki7), (L3 << 4) | J7},///
	
	{ /* (   	*/ (uint8_t)((WDTO_2S    << 4) |  6),  (uint8_t)((k3 << 4) | ki4), (L3 << 4) | J4},//
	{ /* =   	*/ (uint8_t)((WDTO_500MS << 4) |  1),  (uint8_t)((k5 << 4) | ki1), (L5 << 4) | J1},//
	{ /* <   	*/ (uint8_t)((WDTO_2S    << 4) |  1),  (uint8_t)((k3 << 4) | ki7), (L3 << 4) | J7},///
	
	{ /* (   	*/ (uint8_t)((WDTO_2S    << 4) |  6),  (uint8_t)((k3 << 4) | ki4), (L3 << 4) | J4},//
	{ /* =   	*/ (uint8_t)((WDTO_500MS << 4) |  1),  (uint8_t)((k5 << 4) | ki1), (L5 << 4) | J1},//
	{ /* <   	*/ (uint8_t)((WDTO_250MS << 4) |  1),  (uint8_t)((k3 << 4) | ki7), (L3 << 4) | J7},///
	
	{ /* Shift  */ (uint8_t)((WDTO_250MS << 4) |  1),  (uint8_t)((k1 << 4) | ki8), (L1 << 4) | J8},//
	{ /* 9   	*/ (uint8_t)((WDTO_250MS << 4) |  1),  (uint8_t)((k3 << 4) | ki3), (L3 << 4) | J3},//
	{ /* 1   	*/ (uint8_t)((WDTO_250MS << 4) |  1),  (uint8_t)((k1 << 4) | ki1), (L1 << 4) | J1},//
	{ /* =   	*/ (uint8_t)((WDTO_250MS << 4) |  1),  (uint8_t)((k5 << 4) | ki1), (L5 << 4) | J1},//
	{ /* AC   	*/ (uint8_t)((WDTO_250MS << 4) |  1),  (uint8_t)((k7 << 4) | ki3), (L7 << 4) | J3},//
	{ /* Shift  */ (uint8_t)((WDTO_250MS << 4) |  1),  (uint8_t)((k1 << 4) | ki8), (L1 << 4) | J8},//
	{ /* 9   	*/ (uint8_t)((WDTO_250MS << 4) |  1),  (uint8_t)((k3 << 4) | ki3), (L3 << 4) | J3},//
	{ /* 2   	*/ (uint8_t)((WDTO_250MS << 4) |  1),  (uint8_t)((k2 << 4) | ki1), (L2 << 4) | J1},//
	{ /* =   	*/ (uint8_t)((WDTO_250MS << 4) |  1),  (uint8_t)((k5 << 4) | ki1), (L5 << 4) | J1},//
	{ /* AC   	*/ (uint8_t)((WDTO_250MS << 4) |  1),  (uint8_t)((k7 << 4) | ki3), (L7 << 4) | J3},//
	{ /* MODE   */ (uint8_t)((WDTO_250MS << 4) |  2),  (uint8_t)((k7 << 4) | ki8), (L7 << 4) | J8},//
	{ /* Shift  */ (uint8_t)((WDTO_250MS << 4) |  1),  (uint8_t)((k1 << 4) | ki8), (L1 << 4) | J8},//
	{ /* MODE   */ (uint8_t)((WDTO_250MS << 4) |  1),  (uint8_t)((k7 << 4) | ki8), (L7 << 4) | J8},//
	{ /* ˅   	*/ (uint8_t)((WDTO_250MS << 4) |  1),  (uint8_t)((k4 << 4) | ki7), (L4 << 4) | J7},//
	{ /* 6   	*/ (uint8_t)((WDTO_250MS << 4) |  1),  (uint8_t)((k3 << 4) | ki2), (L3 << 4) | J2},//
	{ /* <   	*/ (uint8_t)((WDTO_250MS << 4) | 10),  (uint8_t)((k3 << 4) | ki7), (L3 << 4) | J7},//
	{ /* AC   	*/ (uint8_t)((WDTO_250MS << 4) |  1),  (uint8_t)((k7 << 4) | ki3), (L7 << 4) | J3},//
	{ /* Shift  */ (uint8_t)((WDTO_250MS << 4) |  1),  (uint8_t)((k1 << 4) | ki8), (L1 << 4) | J8},//
	{ /* MODE   */ (uint8_t)((WDTO_250MS << 4) |  1),  (uint8_t)((k7 << 4) | ki8), (L7 << 4) | J8},//
	{ /* 2   	*/ (uint8_t)((WDTO_250MS << 4) |  1),  (uint8_t)((k2 << 4) | ki1), (L2 << 4) | J1},//
	{ /* AC   	*/ (uint8_t)((WDTO_250MS << 4) |  1),  (uint8_t)((k7 << 4) | ki3), (L7 << 4) | J3},//
};

const uint8_t comandos_size = sizeof(comandos)/sizeof(Command);
