#!/bin/bash

# Description
# This script will generate an array of structs according to the keymap made of
# the calculator and the board made for it's hack:
# 
# struct Command
# {
# 	uint8_t time_repeat;       ///< Repeat number lower nibble, time in upper one
# 	volatile uint8_t* row_port;///< Pointer to the pin port of the column.
# 	volatile uint8_t* col_port;///< Pointer to the pin port of the row.
# 	uint8_t rc_pin;            ///< Row pin on upper nibble, col pin on lower one
# };
# 

#List of row/col equivalents
declare -a k1=("PORTB" "PB3")
declare -a k2=("PORTC" "PC5")
declare -a k3=("PORTC" "PC4")
declare -a k4=("PORTC" "PC3")
declare -a k5=("PORTC" "PC2")
declare -a k6=("PORTC" "PC1")
declare -a k7=("PORTC" "PC0")
declare -a ki1=("PORTB" "PB0")
declare -a ki2=("PORTB" "PB1")
declare -a ki3=("PORTB" "PB2")
declare -a ki4=("PORTB" "PB6")
declare -a ki5=("PORTD" "PD4")
declare -a ki6=("PORTD" "PD5")
declare -a ki7=("PORTD" "PD6")
declare -a ki8=("PORTD" "PD7")

#Definition of the PORTS
PORTB="0x05"
PORTC="0x08"
PORTD="0x0B"

#Copy the file
cat pin_template.h > src/keys_comb.h

#Definitios
K=(${k1[0]} ${k2[0]} ${k3[0]} ${k4[0]} ${k5[0]} ${k6[0]} ${k7[0]})
KI=(${ki1[0]} ${ki2[0]} ${ki3[0]} ${ki4[0]} ${ki5[0]} ${ki6[0]} ${ki7[0]} ${ki8[0]})
L=(${k1[1]} ${k2[1]} ${k3[1]} ${k4[1]} ${k5[1]} ${k6[1]} ${k7[1]})
J=(${ki1[1]} ${ki2[1]} ${ki3[1]} ${ki4[1]} ${ki5[1]} ${ki6[1]} ${ki7[1]} ${ki8[1]})

#The next two loops are the PORTS
#First loop. 
for i in {1..7}
do
	sed "s/k$i/${K[$i-1]}/g" -i src/keys_comb.h
	sed "s/L$i/${L[$i-1]}/g" -i src/keys_comb.h
done

#Second loop
for j in {1..8}
do
	sed "s/ki$j/${KI[$j-1]}/g" -i src/keys_comb.h
	sed "s/J$j/${J[$j-1]}/g" -i src/keys_comb.h
done

#Formating
sed 's/\, /\,/g' src/keys_comb.h -i
sed 's/\,/\, /g' src/keys_comb.h -i

#Replace port values
sed "s/PORTB/${PORTB}/g" src/keys_comb.h -i
sed "s/PORTC/${PORTC}/g" src/keys_comb.h -i
sed "s/PORTD/${PORTD}/g" src/keys_comb.h -i


